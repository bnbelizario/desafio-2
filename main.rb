load 'inscricao.rb'
load 'sala.rb'
load 'alocador.rb'
require 'pry'
require 'csv'
require 'awesome_print'

idiomas = %w[Inglês Alemão Espanhol Klingon]
inscricoes = Inscricao.new('data/inscricoes.csv', idiomas)

salas = Sala.new('data/salas.csv')

# puts inscricoes.total_por_idiomas
# ap inscricoes.por_idiomas
# ap inscricoes.all

# puts salas.all


alocacoes = Alocador.new(inscricoes, salas)

# puts alocacoes.alocar_salas
# puts alocacoes.alocar_alunos
#
alocacoes.alocar_alunos.each do |alocacao|
  puts "Sala #{alocacao[:sala]}, #{alocacao[:horario]}, #{salas.find(alocacao[:sala])[:vagas] } lugares, #{alocacao[:idioma]}, Dia: #{alocacao[:dia]} \n"
  # alocacao[:alunos].each do |aluno|
  #   puts "#{aluno[:nome]}\n"
  # end
end

ap inscricoes.alunos_com_mais_de_uma_inscricao

# inscricoes.alunos_com_mais_de_uma_inscricao.each do |aluno|
#   puts "#{aluno[:aluno]},#{aluno[:idiomas]}"
# end
# ap inscricoes.idiomas_por_alunos


