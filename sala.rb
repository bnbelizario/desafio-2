class Sala
  def initialize(sala_path)
    @sala_file = CSV.read(sala_path, headers: true)
    @all = all
  end

  def all
    @salas = []
    @sala_file.each { |sala| @salas << sala.to_h }
    # Transformando as chaves em símbolos dado que não temos symbolize_keys
    @salas.map! { |i| i.each_with_object({}) { |h, (k, v)| h[k.to_sym] = v; h } }
    @salas.each { |sala| sala[:vagas] = sala[:vagas].to_i }
  end

  def find(nome)
    all.select { |sala| sala[:nome] == nome }[0]
  end
end
