class Inscricao
  attr_reader :idiomas
  def initialize(inscricoes_path, idiomas = %w[Inglês Alemão Espanhol Klingon])
    @inscricoes_file = CSV.read(inscricoes_path, headers: true)
    @all = all
    @idiomas = idiomas
  end

  def all
    @inscricoes = []
    @inscricoes_file.each { |inscricao| @inscricoes << inscricao.to_h }
    # Transformando as chaves em símbolos dado que não temos symbolize_keys
    @inscricoes.map { |i| i.each_with_object({}) { |h, (k, v)| h[k.to_sym] = v; h } }
  end

  def total_por_idiomas
    hash = {}
    @idiomas.each do |idioma|
      count = 0
      all.each { |i| count += 1 if i[:idioma].eql? idioma }
      hash.merge!({ idioma => count })
    end
    hash
  end

  def idiomas_por_alunos
    alunos = []
    all.each do |inscricao|
      idiomas = all.select { |i| i[:nome] == inscricao[:nome] }
      idiomas.map! { |idioma| idioma[:idioma] }
      hash = { aluno: inscricao[:nome], idiomas: idiomas }
      alunos << hash
    end
    alunos
  end

  def por_idiomas
    hash = {}
    @idiomas.each do |idioma|
      hash.merge!({ idioma => [] })
      all.each do |i|
        hash[idioma] << { nome: i[:nome] } if i[:idioma].eql? idioma
      end
    end
    hash
  end

  def alunos_com_mais_de_uma_inscricao
    idiomas_por_alunos.select { |aluno| aluno[:idiomas].size > 1 }.uniq!
  end
end
