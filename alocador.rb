class Alocador
  def initialize(inscricoes, salas)
    @inscricoes = inscricoes
    @salas = salas.all
    @horarios = %w[Manhã Tarde]
    @dias = [1, 2]
  end

  def alocar_salas
    alocacao = []
    @inscricoes.total_por_idiomas.each do |insc|
      hash = { idioma: insc[0], sala: sala_alocada_para_idioma(@salas, insc),
               horario: nil, dia: nil }
      count = alocacao.reject { |l| l[:horario].nil? }.size
      hash[:horario] = @horarios[0]
      hash[:horario] = @horarios[1] if count >= 2
      hash[:dia] = @dias[0]
      alocacao << hash
      @salas.delete_if do |sala|
        count = alocacao.select { |s| s[:sala] == sala[:nome] }.size
        count >= 2
      end
    end
    alocacao
  end

  def alocar_alunos
    alocar_salas.each do |sala|
      alunos = @inscricoes.all.select do |inscricao|
        inscricao[:idioma] == sala[:idioma]
      end
      hash = { alunos: alunos }
      sala.merge!(hash)
    end
  end

  private

  def sala_alocada_para_idioma(salas, inscricao)
    diferenca_inscritos_vagas(salas, inscricao).min_by { |_, v| v }[0]
  end

  def diferenca_inscritos_vagas(salas, inscricao)
    diferenca = {}
    salas.each do |sala|
      diferenca.merge!({ sala[:nome] => sala[:vagas] - inscricao[1] })
    end
    diferenca.keep_if { |_, v| v.positive? }
  end
end
